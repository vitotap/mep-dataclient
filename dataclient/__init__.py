"""
The mep_dataclient uses the `PROBA-V MEP <https://proba-v-mep.esa.int/>`_ web services to download remote sensing data.
This client is aimed at retrieving specific regions of interest, bounded by a polygon. It uses externally accessible web services
so it is not dependent on the PROBA-V MEP environment.
"""

__title__ = 'mep_dataclient'
__version__ = '0.3.5'
__author__ = 'Jeroen Dries'

from .api import probav_s10_toc_300m, MepLayer, s2_fapar_10m, s2_fcover_10m, s2_lai_10m, s2_ndvi_10m, s2_radiometry_10m

from .tsservice import get_timeseries, get_timeseries_n_features, get_timeseries_multiband, get_histogram