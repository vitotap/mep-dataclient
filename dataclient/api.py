import collections
import json
import math
import os
import pkgutil
import tempfile
from xml.etree import ElementTree

import pandas as pd
import pytz
import requests
from dateutil.parser import parse
from decimal import *


def probav_s10_toc_300m(credentials):
    """Returns layer object giving access to PROBA-V S10 TOC 300M NDVI data."""
    return MepLayer(credentials, "PV_MEP__PROBAV_S10_TOC_333M_NDVI", "PV_MEP",
                    "https://proba-v-mep.esa.int/applications/geo-viewer/app/geoserver/ows")


def s2_fapar_10m(credentials):
    return MepLayer(credentials, "CGS__CGS_S2_FAPAR", "CGS", "https://sentineldata.vito.be/ows","S2_FAPAR")

def s2_fcover_10m(credentials):
    return MepLayer(credentials, "CGS__CGS_S2_FCOVER", "CGS", "https://sentineldata.vito.be/ows","S2_FCOVER")

def s2_lai_10m(credentials):
    return MepLayer(credentials, "CGS__CGS_S2_LAI", "CGS", "https://sentineldata.vito.be/ows","S2_LAI")

def s2_ndvi_10m(credentials):
    return MepLayer(credentials, "CGS__CGS_S2_NDVI", "CGS", "https://sentineldata.vito.be/ows","S2_NDVI")

def s2_radiometry_10m(credentials):
    return MepLayer(credentials, "CGS__CGS_S2_10M_BANDS", "CGS", "http://mapcachedev.vgt.vito.be/geoserver/ows","S2_RADIOMETRY")

CoverageInfo = collections.namedtuple('CoverageInfo', ['times', 'xAxis', 'yAxis', 'timeAxis', 'srs', 'nodata','pixelsize'])


class MepLayer:
    """This class represents a data layer. It contains mostly metadata, and provides data retrieval methods."""
    wcsLayerName = None
    url = None
    border_crop_S2 = 0

    def getCoverageInfo(self, id, workspace, url ):
        payload = {
            'service': 'WCS',
            'request': 'DescribeCoverage',
            'version': '2.0.1',
            'coverageId': id
        }

        response = requests.get(url, params=payload)
        if response.status_code is not 200:
            raise IOError("GetCoverageInfo request to " +url + "failed: " + response.text)
        try:
            tree = ElementTree.fromstring(response.content)
        except ElementTree.ParseError as e:
            raise RuntimeError("Failed to parse Coverage Info: " + str(response.content) + "\n Error: " + str(e.msg))
        namespaces = {
            'wcs2': 'http://www.opengis.net/wcs/2.0',
            'wcsgs': 'http://www.geoserver.org/wcsgs/2.0',
            'gmlcov': 'http://www.opengis.net/gmlcov/1.0',
            'gml': 'http://www.opengis.net/gml/3.2',
            'swe': 'http://www.opengis.net/swe/2.0'
        }

        times = [ x.text for x in tree.findall(
            'wcs2:CoverageDescription/gmlcov:metadata/gmlcov:Extension/wcsgs:TimeDomain/gml:TimeInstant/gml:timePosition',
            namespaces)]
        axis_labels = tree.find('wcs2:CoverageDescription/gml:boundedBy/gml:EnvelopeWithTimePeriod', namespaces)
        names = axis_labels.attrib['axisLabels'].split()
        noDataElement = tree.find('wcs2:CoverageDescription/gmlcov:rangeType/swe:DataRecord/swe:field/swe:Quantity/swe:nilValues/swe:NilValues/swe:nilValue', namespaces)
        nodata = None
        if noDataElement is not None:
            nodata = float(noDataElement.text)
        srs = int(axis_labels.attrib['srsName'].split('/')[-1])
        x_axis_name = names[0]
        y_axis_name = names[1]
        time_axis = names[2]
        offsetVector = tree.find('wcs2:CoverageDescription/gml:domainSet/gml:RectifiedGrid/gml:offsetVector[1]',namespaces).text
        v1 = Decimal(offsetVector.split()[0])
        v2 = Decimal(offsetVector.split()[1])
        pixelsize = v1.copy_abs()
        if pixelsize == 0.:
            pixelsize = v2.copy_abs()


        return CoverageInfo(times=times, xAxis=x_axis_name, yAxis=y_axis_name, timeAxis=time_axis, srs=srs, nodata=nodata, pixelsize = pixelsize)

    def __init__(self, credentials, wcsLayerName, workspace, url,tsservice_layer_id=None):
        self.tsservice_layer_id = tsservice_layer_id
        self.wcsLayerName = wcsLayerName
        self.url = url
        self.coverageInfo = self.getCoverageInfo(wcsLayerName, workspace, url)
        self.credentials = credentials
        self.__load_template()

    def timeseries(self, geodataframe,startdate,enddate,threads = 1,single_request=False,endpoint = None):
        """
        Retrieves all timeseries corresponding to features in a GeoPandas dataframe.

        :param geodataframe: A Geopandas dataframe containing polygons to query.
        :param startdate: The start date of the timeseries to query
        :param enddate: The last date of the timeseries to query.

        :returns A dataframe containing a timeseries for each feature in the original dataframe.

        """
        from .tsservice import TSService
        if endpoint is None:
            endpoint = TSService.DEFAULT_ENDPOINT
        if self.tsservice_layer_id is None:
            raise NotImplementedError("Retrieving timeseries for this layer is not implemented: "+self.wcsLayerName)
        return TSService(endpoint=endpoint).timeseries(self.tsservice_layer_id,geodataframe,startdate,enddate,threads,single_request=single_request)

    def __load_template(self):
        # if "PROBAV" in self.wcsLayerName:
        #    self.template = open("probamasked.xml",'rb').read()
        # else:
        self.template = pkgutil.get_data("dataclient", "doCrop.xml").decode("utf-8")
        self.wcs_template = pkgutil.get_data("dataclient", "wcs_get_coverage.xml").decode("utf-8")

    def __warpField(self, polygonDict, srs):
        import ogr, osr
        if type(polygonDict) is dict:
            polygon = ogr.CreateGeometryFromJson(json.dumps(polygonDict))
        elif 'wkt' in dir(polygonDict):
            polygon = ogr.CreateGeometryFromWkt(polygonDict.wkt)
        else:
            raise ValueError("Unsupported geometry type: "+ polygonDict)

        # create coordinate transformation

        in_spatial_ref = osr.SpatialReference()
        in_spatial_ref.ImportFromEPSG(4326)
        out_spatial_ref = osr.SpatialReference()
        out_spatial_ref.ImportFromEPSG(srs)
        coord_transform = osr.CoordinateTransformation(in_spatial_ref, out_spatial_ref)
        # transform polygon
        polygon.Transform(coord_transform)
        geojson = polygon.ExportToGML(["FORMAT=GML3", "GML3_LONGSRS=NO"])
        # polygon.ExportToJson()

        bbox = polygon.GetEnvelope()
        return (bbox, geojson)

    def __round_to_resolution(self,value):
        """
        Rounds a decimal coordinate value to a value so that it is accurate up to 1/100 of the pixel size.
        This rounding avoids generating floating point coordinates with an arbitrary (system specific) number of digits.  
        :param value: 
        :param resolution: 
        :return: 
        """
        resolution = self.coverageInfo.pixelsize
        max_error = (resolution / Decimal("100"))
        return Decimal(value / max_error).quantize(Decimal('1.'), rounding=ROUND_HALF_DOWN) * max_error

    def __configureCoverage(self, coverageXML, bbox, date, geojson = "", coverageId=None):
        if coverageXML is None:
            return
        ns = {'gml': 'http://www.opengis.net/gml',
              'wps': 'http://www.opengis.net/wps/1.0.0',
              'wcs': 'http://www.opengis.net/wcs/2.0',
              'wcs2': 'http://www.opengis.net/wcs/2.0',
              'ows': 'http://www.opengis.net/ows/1.1'}
        x_axis_name = self.coverageInfo.xAxis
        y_axis_name = self.coverageInfo.yAxis
        time_axis = self.coverageInfo.timeAxis
        if coverageId is not None:
            coverageXML.find('wcs2:CoverageId', ns).text = coverageId
        coverageXML.find('wcs2:DimensionTrim[1]/wcs2:Dimension',
                         ns).text = y_axis_name if "4326" in geojson else x_axis_name
        #bounding box has to be a bit larger than cropShape, otherwise cropping on polygon is not applied
        coverageXML.find('wcs2:DimensionTrim[1]/wcs2:TrimLow', ns).text = str(self.__round_to_resolution(Decimal(bbox[0])-self.coverageInfo.pixelsize))
        coverageXML.find('wcs2:DimensionTrim[1]/wcs2:TrimHigh', ns).text = str(self.__round_to_resolution(Decimal(bbox[1])+self.coverageInfo.pixelsize))
        coverageXML.find('wcs2:DimensionTrim[2]/wcs2:Dimension',
                         ns).text = x_axis_name if "4326" in geojson else y_axis_name
        coverageXML.find('wcs2:DimensionTrim[2]/wcs2:TrimLow', ns).text = str(self.__round_to_resolution(Decimal(bbox[2])-self.coverageInfo.pixelsize))
        coverageXML.find('wcs2:DimensionTrim[2]/wcs2:TrimHigh', ns).text = str(self.__round_to_resolution(Decimal(bbox[3])+self.coverageInfo.pixelsize))

        coverageXML.find('wcs2:DimensionSlice/wcs2:Dimension', ns).text = time_axis
        coverageXML.find('wcs2:DimensionSlice/wcs2:SlicePoint', ns).text = date

    def __getTemplate(self, geojson, bbox, date, crop_distance_pixels=0):
        templateWithPolygon = self.template.replace("GEOJSON_INPUT", geojson)
        ns = {'gml': 'http://www.opengis.net/gml',
              'wps': 'http://www.opengis.net/wps/1.0.0',
              'wcs': 'http://www.opengis.net/wcs/2.0',
              'wcs2': 'http://www.opengis.net/wcs/2.0',
              'ows': 'http://www.opengis.net/ows/1.1'}
        templateXML = ElementTree.fromstring(templateWithPolygon)

        self.__configureCoverage(templateXML.find(
            'wps:DataInputs/wps:Input[ows:Identifier=\'coverage\']/wps:Reference/wps:Body/wcs2:GetCoverage', ns), bbox,
                                 date, geojson, coverageId=self.wcsLayerName)
        self.__configureCoverage(templateXML.find(
            'wps:DataInputs/wps:Input[ows:Identifier=\'coverageA\']/wps:Reference/wps:Body/wps:Execute/wps:DataInputs/wps:Input[ows:Identifier=\'coverage\']/wps:Reference/wps:Body/wcs:GetCoverage',
            ns), bbox, date, geojson, coverageId=self.wcsLayerName)
        maskCoverage = templateXML.find(
            'wps:DataInputs/wps:Input[ows:Identifier=\'coverageB\']/wps:Reference/wps:Body/wps:Execute/wps:DataInputs/wps:Input[ows:Identifier=\'coverage\']/wps:Reference/wps:Body/wcs:GetCoverage',
            ns)
        if maskCoverage is not None:
            self.__configureCoverage(maskCoverage, bbox, date, geojson)

        templateXML.find('wps:DataInputs/wps:Input[ows:Identifier=\'cropShape\']/wps:Reference/wps:Body/wps:Execute/wps:DataInputs/wps:Input[ows:Identifier=\'distance\']/wps:Data/wps:LiteralData',ns).text='%.6g' % (crop_distance_pixels * self.coverageInfo.pixelsize)

        requestString = ElementTree.tostring(templateXML)
        return requestString

    def __getWCSTemplate(self,  geojson,bbox, date,buffer_distance=0):
        """

        :param geojson:
        :param bbox:
        :param date:
        :param buffer_distance: UNSUPPORTEDD
        :return:
        """
        templateWithPolygon = self.wcs_template
        ns = {'gml': 'http://www.opengis.net/gml',
              'wps': 'http://www.opengis.net/wps/1.0.0',
              'wcs': 'http://www.opengis.net/wcs/2.0',
              'wcs2': 'http://www.opengis.net/wcs/2.0',
              'ows': 'http://www.opengis.net/ows/1.1'}
        templateXML = ElementTree.fromstring(templateWithPolygon)

        self.__configureCoverage(templateXML, bbox,date, geojson, coverageId=self.wcsLayerName)

        requestString = ElementTree.tostring(templateXML)
        return requestString

    def __saveTemp(self, r, name):
        with open(name, "wb") as tempfile:
            tempfile.write(r.content)


    def download_bbox(self, path, polygon, startdate=False, enddate=False, verbose=False):
        """Download data for the bounding box of apolygon and a given time range, to a user specified location.

        :param polygon: a Polygon, can be either a Shapely geometry, or a dict in geojson format. Should be specified in EPSG:4326.
        """
        request_query = "?service=WCS&request=GetCoverage"
        template_method = self.__getWCSTemplate
        self.__invoke_ogc_service(path, polygon, startdate, enddate, request_query, template_method,
                                  0, verbose)


    def download(self, path, polygon, startdate=False, enddate=False, verbose=False, buffer_distance_pixels = 0):
        """Download data for a given polygon and time range, to a user specified location.
        The data will be masked based on the given polygon.
        The polygon can be buffered using buffer_distance_pixels, can be negative to shrink the polygon.
        :param polygon: a Polygon, can be either a Shapely geometry, or a dict in geojson format. Should be specified in EPSG:4326.
        """
        request_query = "?service=WPS&request=execute"
        template_method = self.__getTemplate
        self.__invoke_ogc_service(path, polygon, startdate, enddate, request_query, template_method,
                                  buffer_distance_pixels, verbose)

    def __invoke_ogc_service(self, path, polygon, startdate, enddate, request_query, template_method,
                             buffer_distance_pixels, verbose):
        (bbox, geojson) = self.__warpField(polygon, self.coverageInfo.srs)
        basedir = path
        if not os.path.exists(basedir):
            os.makedirs(basedir)
        startdate = MepLayer.convert_date(startdate)
        enddate = MepLayer.convert_date(enddate)
        results = {}
        for t in self.coverageInfo.times:

            the_date = MepLayer.convert_date(t)
            if (not startdate or the_date >= startdate) and (not enddate or the_date <= enddate):
                if verbose:
                    print("query date: " + t)

                request = template_method(geojson, bbox, t, buffer_distance_pixels)
                if verbose:
                    print(request)
                r2 = requests.post(self.url + request_query, data=request, auth=self.credentials)
                if r2.headers['Content-Type'] == 'image/tiff':
                    outFile = os.path.join(basedir, self.wcsLayerName + "-" + the_date.strftime('%y%m%d') + ".tiff")
                    self.__saveTemp(r2, outFile)
                else:
                    print(r2.text)

    def pixels(self, polygon, startdate=False, enddate=False, verbose=False,buffer_distance_pixels = 0):
        """Retrieve pixels for a given polygon and time range, returns a pandas dataframe.
            The polygon can be buffered using buffer_distance_pixels, can be negative to shrink the polygon.
            :param polygon: a Polygon, can be either a Shapely geometry, or a dict in geojson format.
        """
        template_method = self.__getTemplate
        import pandas as pd
        import numpy as np
        import rasterio
        (bbox, geojson) = self.__warpField(polygon, self.coverageInfo.srs)

        startdate = MepLayer.convert_date(startdate)
        enddate = MepLayer.convert_date(enddate)
        df = pd.DataFrame()
        results = {}
        for t in self.coverageInfo.times:

            theDate = MepLayer.convert_date(t)
            if (not startdate or theDate >= startdate) and (not enddate or theDate <= enddate):
                print("query date: " + t)
                shortDate = theDate.strftime('%m-%d')

                # print(request)
                request = template_method(geojson, bbox, t, buffer_distance_pixels)
                if verbose:
                    print(request)
                r2 = requests.post(self.url + "?service=WPS&request=execute", data=request, auth=self.credentials)
                if r2.headers['Content-Type'] == 'image/tiff':
                    fd, outFile = tempfile.mkstemp(prefix=self.wcsLayerName + "-" + theDate.strftime('%y%m%d') ,suffix=".tiff")
                    self.__saveTemp(r2, outFile)
                    with rasterio.open(outFile) as src:
                        band = src.read()
                        if band.min() != self.coverageInfo.nodata:
                            results[shortDate] = outFile
                            df[theDate] = np.ravel(band)
                    try:
                        os.close(fd)
                        os.remove(outFile)
                    except OSError:
                        pass
                else:

                    print(r2.text)

        df = df.astype(np.float)
        df = df.replace(self.coverageInfo.nodata, np.nan)
        return df

    @classmethod
    def convert_date(cls, date):
        if type(date) is str:

            date = pd.to_datetime(date)
            if date.tzinfo == None:
                date = pytz.utc.localize(date)
            return date

        return date
