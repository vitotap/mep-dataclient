import json
import shutil
from functools import partial
from datetime import datetime
import pytz

import pandas as pd
import requests
import shapely.geometry
from shapely.geometry import mapping
from shapely.geometry.base import BaseGeometry





class TSService:
    DEFAULT_ENDPOINT = "https://proba-v-mep.esa.int/api/timeseries/v1.0/ts/"
    """This class provides a Python API for the PROBA-V MEP Time Series Service. https://proba-v-mep.esa.int/api/timeseries/apidocs/"""
    def __init__(self,endpoint = "https://proba-v-mep.esa.int/api/timeseries/v1.0/ts/"):
        self.endpoint = endpoint

    @classmethod
    def _convert_date(cls, date):
        if type(date) is str:
            return pytz.utc.localize(pd.to_datetime(date))

        return date

    def timeseries(self, layername,geodataframe,startdate,enddate,threads = 1,single_request = False):
        """Retrieves all timeseries corresponding to features in a GeoPandas dataframe.

        """
        if(single_request):
            import tempfile
            (handle,path) = tempfile.mkstemp(suffix=".json", prefix="timeseries")
            with open(path,"wb") as out:
                download_timeseries(layername, startdate, enddate, geodataframe, out,endpoint=self.endpoint)
            with open(path,"r") as input_json:
                data = json.load(input_json)

                def replace_nan_string(field):
                    return {k: float('nan') if v == 'NaN' else v for k, v in field.items()}

                dataframe = pd.DataFrame([pd.Series(data=[replace_nan_string(field) for field in fields]
                                                    ,name=pd.to_datetime(date)) for date,fields in data["results"].items()])
                transposed = dataframe.transpose()
                transposed.index = geodataframe.index
                return transposed

        else:
            #reproject to lat lon, which is what our web service uses

            parcels_latlon_df = self.to_lat_lon(geodataframe)

            startdate = TSService._convert_date(startdate)
            enddate = TSService._convert_date(enddate)
            #convert geometry to json
            parcels_latlon_df['geometry_json'] = parcels_latlon_df.geometry.apply(lambda g:json.dumps(mapping(g)))
            return self.retrieve_curves(layername,parcels_latlon_df,pd.to_datetime(startdate),pd.to_datetime(enddate),threads=threads,endpoint=self.endpoint)



    @classmethod
    def to_lat_lon(cls, geodataframe):
        parcels_latlon_df = geodataframe.to_crs(epsg=4326)
        print(parcels_latlon_df.geometry)
        # after transformation, there appear to be some empty polygons, we filter them out
        parcels_latlon_df = parcels_latlon_df[~parcels_latlon_df.geometry.is_empty]
        return parcels_latlon_df


    def timeseries_async(self, layername,geodataframe,startdate,enddate,num_workers = 8):
        """
        Query the timeseries for each feature in a geopandas dataframe, using multithreading to parallellize queries.
        :param layername:
        :param geodataframe:
        :param startdate:
        :param enddate:
        :param num_workers:
        :return: An iterable that returns timeseries per feature.
        """
        #reproject to lat lon, which is what our web service uses
        parcels_latlon_df = self.to_lat_lon(geodataframe)

        import functools
        func = functools.partial(_retrieve_curve,layername,startdate,enddate,endpoint=self.endpoint)
        from multiprocessing import Pool
        pool = Pool(num_workers)
        return pool.imap_unordered(func, parcels_latlon_df.iterrows(),1)

    @classmethod
    def _parallelize_dataframe(cls,df, func,partitions,num_cores):
        import numpy as np
        from multiprocessing import Pool
        #from multiprocess import Pool
        df_split = np.array_split(df, partitions)
        pool = Pool(num_cores)
        df = pd.concat(pool.map(func, df_split))
        pool.close()
        pool.join()
        return df



    @classmethod
    def retrieve_curves(cls, layername, geodataframe, startdate, enddate, threads = 1, endpoint=DEFAULT_ENDPOINT):
        import functools
        func = functools.partial(_local,layername,startdate,enddate,endpoint = endpoint)
        if threads is 1:
            return func(geodataframe)
        else:
            return cls._parallelize_dataframe(geodataframe, func,len(geodataframe)/10,threads)



def _local(layername,startdate,enddate,df,endpoint="https://proba-v-mep.esa.int/api/timeseries/v1.0/ts/"):
    if 'progress_apply' in dir(df):
        return df.progress_apply(partial(_retrieve_curve,layername,startdate,enddate,endpoint=endpoint), axis=1)
    else:
        return df.apply(partial(_retrieve_curve,layername,startdate,enddate,endpoint=endpoint), axis=1)

def _retrieve_curve(layername,startdate,enddate,field_data,endpoint="https://proba-v-mep.esa.int/api/timeseries/v1.0/ts/"):


    url = endpoint+layername+"/geometry"
    service_response, result_id = _post_single_field(field_data, startdate, enddate, url)

    #curve = requests.get(url)
    if(service_response.status_code == 200):
        try:
            if(len(service_response.json()['results']) > 0):
                date = pd.to_datetime([x['date'] for x in service_response.json()['results']])
                values = pd.to_numeric([x['result']['average'] for x in service_response.json()['results']],errors='coerce')
                series = pd.Series(values,index=date,name=result_id)
                return  series
            else:
                print("Empty series for field: "+result_id)
                return None
        except ValueError:
            print("Invalid json")
            print(service_response.text)
            return None
    else:
        print(service_response.text)
        print("Error for: "+str(field_data))
        return None

def get_histogram(feature,layername,startdate,enddate,endpoint="https://proba-v-mep.esa.int/api/timeseries/v1.0/ts/"):
    """
    Retrieves a timeseries of histograms for a geojson feature.

    :param feature:
    :param layername:
    :param startdate:
    :param enddate:
    :param endpoint:
    :return: pandas time series
    :rtype: pd.Series
    """
    curve = _retrieve_histogram(layername,startdate,enddate,feature,endpoint)
    if curve is None:
        raise IOError("Error while retrieving curve for: "+layername)
    return curve

def _retrieve_histogram(layername,startdate,enddate,field_data,endpoint="https://proba-v-mep.esa.int/api/timeseries/v1.0/ts/"):

    url = endpoint+layername+"/geometry/histogram"
    service_response, result_id = _post_single_field(field_data, startdate, enddate, url)

    #curve = requests.get(url)
    if(service_response.status_code == 200):
        try:
            response_json = service_response.json()
            if(len(response_json['results']) > 0):
                def toSeries(json_histogram):
                    data = [bucket['count'] for bucket in json_histogram['result']]
                    index = [float('nan') if bucket['value'] == 'NaN' else bucket['value'] for bucket in json_histogram['result']]
                    return pd.Series(data=data,name=json_histogram['date'],index=index)
                series = [toSeries(row) for row in response_json['results']]

                df = pd.DataFrame(series).fillna(0)
                df.index = pd.to_datetime(df.index)

                return  df
            else:
                print("Empty series for field: "+result_id)
                return None
        except ValueError:
            print("Invalid json")
            print(service_response.text)
            return None
    else:
        print("Received error for url: " + url + " Error message: " + service_response.text)
        print("Error for polygon: " + str(field_data))
        return None


def _post_single_field(field_data, startdate, enddate, url):
    if isinstance(field_data, tuple):
        field_data = field_data[1]
    shape = _find_shape(field_data)
    params = _get_params(startdate, enddate)
    curve = requests.post(url, json=shape, params=params)
    result_id = "unknown"
    if hasattr(field_data, "name"):
        result_id = field_data.name
    elif "id" in field_data:
        result_id = field_data["id"]
    return curve, result_id


def _get_params(startdate, enddate):
    params = {}
    if startdate is not None:
        startdate = TSService._convert_date(startdate)
        params["startDate"] = startdate.date().isoformat()
    if enddate is not None:
        enddate = TSService._convert_date(enddate)
        params["endDate"] = enddate.date().isoformat()
    return params


def _find_shape(field_data):
    if 'geometry_json' in field_data:
        shape = field_data['geometry_json']
        if isinstance(shape, str):
            shape = json.loads(shape)
    elif isinstance(field_data['geometry'], BaseGeometry):
        shape = mapping(field_data['geometry'])
    else:
        shape = field_data['geometry']
    return shape



def download_timeseries(layername, startdate, enddate, features_dataframe, out,endpoint = "https://proba-v-mep.esa.int/api/timeseries/v1.0/ts/"):
    """
    Downloads a timeseries for all features in a geopandas dataframe, for a given date range and layer name. The result is written into a file like object as json.
    This call is optimized for querying a large number of features at the same time.

    :param layername: The layername, a list of available layers can be retrieved: https://proba-v-mep.esa.int/api/timeseries/v1.0/ts
    :param startdate: The start date for the time series, required
    :type startdate: str or datetime
    :param enddate: The end date for the timeseries, required
    :param features_dataframe:
    :type features: gpd.DataFrame
    :param out:
    :type out: file like object
    :param endpoint: The endpoint to use.
    :return: Nothing
    """

    #reproject to lat lon, which is what our web service uses

    parcels_latlon_df = TSService.to_lat_lon(features_dataframe)

    startdate = TSService._convert_date(startdate)
    enddate = TSService._convert_date(enddate)

    import tempfile
    import os
    tick = datetime.now()
    (handle, geojson_lat_lon) = tempfile.mkstemp(suffix=".geojson", prefix="dataclient")
    try:
        #Windows requires closing this file before opening it elsewhere
        os.close(handle)
    except OSError:
        pass
    parcels_latlon_df.to_file(geojson_lat_lon, driver="GeoJSON")
    headers = {'content-type': 'application/json'}
    print("Start sending features to webservice.")
    with open(geojson_lat_lon, "rb") as file:
        url = endpoint + layername + "/features?endDate=" + enddate.date().isoformat() + "&startDate=" + startdate.date().isoformat()
        print(url)
        with requests.post(url, stream=True, data=file, headers=headers) as result:
            print("Start receiving result.")
            tock = datetime.now()
            diff = tock - tick
            print(str(diff.seconds) + " seconds to send request.")
            import os
            os.remove(geojson_lat_lon)
            if (result.status_code == 200):

                shutil.copyfileobj(result.raw, out)
            else:
                raise IOError("Error while invoking web service: " + result.text)

def get_curve(feature,layername,startdate=None,enddate=None,endpoint="https://proba-v-mep.esa.int/api/timeseries/v1.0/ts/"):
    """
    Retrieves a timeseries curve for a geojson feature.
    Deprecated, use #get_timeseries

    :param feature:
    :param layername:
    :param startdate:
    :param enddate:
    :param endpoint:
    :return: pandas time series
    :rtype: pd.Series
    """
    curve = _retrieve_curve(layername, startdate, enddate, feature, endpoint)
    if curve is None:
        raise IOError("Error while retrieving curve for: "+layername)
    return curve

def get_timeseries(feature,layername,startdate=None,enddate=None,endpoint="https://proba-v-mep.esa.int/api/timeseries/v1.0/ts/"):
    """
    Retrieves a timeseries for a geojson feature.

    :param feature:
    :param layername:
    :param startdate:
    :param enddate:
    :param endpoint:
    :return: pandas time series
    :rtype: pd.Series
    """
    return get_curve(feature,layername,startdate,enddate,endpoint)

def _get_multiband_layer_bandnames(layername, endpoint, bands):
    # TODO: remove these hardcoded lists in the future!  (see https://jira.vito.be/browse/EP-2933)
    if layername == 'S1_GRD_SIGMA0_ASCENDING':
        return ['VH', 'VV', 'ANGLE']
    elif layername == 'S1_GRD_SIGMA0_DESCENDING':
        return ['VH', 'VV', 'ANGLE']
    else:
        return [str(i) for i in range(bands)]

def get_timeseries_multiband(feature, layername, startdate, enddate, endpoint="https://proba-v-mep.esa.int/api/timeseries/v1.0/ts/"):

    # Send a request to the timeseries web service

    url = endpoint + layername + '/geometry/multiband'

    service_response, result_id = _post_single_field(feature, startdate, enddate, url)

    if service_response.status_code == 200:
        try:
            # Accepted, now read and parse the result as JSON.
            #
            # Multiband results contain multiple value dicts per date,
            # one dict per band.  For example:
            #
            # {
            #   'results': [{
            #       'date': '2017-01-01',
            #       'result': [
            #         {'totalCount': 0, 'validCount': 0, 'average': 0.0},
            #         {'totalCount': 0, 'validCount': 0, 'average': 0.0},
            #         {'totalCount': 0, 'validCount': 0, 'average': 0.0}
            #       ]
            #     }, {
            #       'date': '2017-01-01',
            #       'result': [
            #         {'totalCount': 0, 'validCount': 0, 'average': 0.0},
            #         {'totalCount': 0, 'validCount': 0, 'average': 0.0},
            #         {'totalCount': 0, 'validCount': 0, 'average': 0.0}
            #       ]
            #     }
            #     ...
            #   ]
            # }

            results = service_response.json()['results']

            if len(results) > 0:

                # Convert this to a dict of pandas Series, one per band:
                #
                # {
                #   'VV': pd.Series(...),
                #   'VH': pd.Series(...),
                #   'ANGLE': pd.Series(...)
                # }

                dates = pd.to_datetime([x['date'] for x in results])
                values = pd.to_numeric([[y['average'] for y in x['result']]
                                        for x in results],
                                       errors='coerce')

                bandnames = _get_multiband_layer_bandnames(layername,
                                                           endpoint,
                                                           values.shape[1])

                series = {bandnames[i]: pd.Series(data=values[:,i],
                                                  index=dates,
                                                  name=result_id)
                          for i in range(values.shape[1])}

                return series
            else:
                print("Empty series for field: "+result_id)
                raise IOError("Error while retrieving curve for: " + layername)
        except ValueError:
            print("Invalid json")
            print(service_response.text)
            raise IOError("Error while retrieving curve for: " + layername)
    else:
        print(service_response.text)
        print("Error for: "+str(feature))
        raise IOError("Error while retrieving curve for: " + layername)

def get_timeseries_n_features(features_df, layername, startdate, enddate, endpoint = "https://proba-v-mep.esa.int/api/timeseries/v1.0/ts/"):
    """
    Downloads a timeseries for all features in a geopandas dataframe, for a given date range and layer name.
    This call is optimized for querying a large number of features at the same time, but the user should ensure that the
    result fits in memory.
    For larger inputs or results, use #download_timeseries.

    :param features_df: A geopandas dataframe containing all features.
    :param layername: The layer to query
    :param startdate: The start date for the time series, required
    :param enddate: The end date for the time series, required
    :param endpoint: End point to use, optional.
    :return: A dict containing one pandas Series per feature
    """

    # Convert the features to lat/lon (Note: this removes empty features!!!)

    df = TSService.to_lat_lon(features_df)

    # Convert dates to datetime date objects

    startdate = TSService._convert_date(startdate).date().isoformat()
    enddate = TSService._convert_date(enddate).date().isoformat()

    # Send a request to the timeseries web service

    headers = {'content-type': 'application/json'}
    url = endpoint + layername + "/features?endDate=" + enddate + "&startDate=" + startdate
    json = df.to_json()

    with requests.post(url, stream=True, data=json, headers=headers) as result:
        if (result.status_code == 200):

            # Accepted, now read and parse the result as JSON (streaming).

            data = result.json()

            # Extract a timeseries dict per feature, for example:
            #
            # {
            #   'feature-1': {
            #     '2017-01-01': 0.0,
            #     '2017-01-02': 0.0,
            #     '2017-01-03': NaN,
            #    },
            #   'feature-2': {
            #     '2017-01-01': 0.0,
            #     '2017-01-02': 0.0,
            #     '2017-01-03': NaN,
            #    },
            # }

            results = {id: {} for id in df.index}

            for date, infolist in data['results'].items():
                for id, info in zip(df.index.values, infolist):

                    # Store the 'average' field as a float, unless a
                    # 'totalCount' field exists and has value 0

                    if info.get('totalCount', 1) > 0:
                        results[id][date] = float(info['average'])

            # Convert timeseries results to pandas series objects.
            #
            # Empty series will instead be set to None, this can be due to:
            #  - an empty feature (skipped in the query)
            #  - no data in the specified date range

            series = {id: None for id in features_df.index}

            for id, ts in results.items():
                if ts:
                    series[id] = pd.Series(name=id, data=ts)
                    series[id].index = pd.to_datetime(series[id].index)

            return series

        else:
            raise IOError("Error while invoking web service: " + result.text)

def layers(endpoint="https://proba-v-mep.esa.int/api/timeseries/v1.0/ts/"):
    response = requests.get(endpoint)
    if response.status_code == 200:
        layerlist = response.json()['layers']
        return layerlist
    else:
        raise IOError("Error while retrieving list of layers."+response.json())

def get_histogram_n_features(features_df, layername, startdate, enddate, endpoint = "https://proba-v-mep.esa.int/api/timeseries/v1.0/ts/"):
    """
    Computes histogram time series given a collection of geometries and a product.
    This call is optimized for querying a small number of features, near to another, at the same time (e.g. 3 buffers around some roi)
    :param features_df: A geopandas dataframe containing all features.
    :param layername: The layer to query
    :param startdate: The start date for the time series, required
    :param enddate: The end date for the time series, required
    :param endpoint: End point to use, optional.
    :return: A dict containing one pandas dataframe per feature. the dataframes countain the histograms (counts per bin)
    """
    dictofdataframes = _retrieve_histogram_n_features(features_df, layername, startdate, enddate, endpoint)
    if dictofdataframes is None:
        raise IOError("Error while retrieving histograms for: " + layername)
    return dictofdataframes

def _retrieve_histogram_n_features(features_df, layername, startdate, enddate, endpoint = "https://proba-v-mep.esa.int/api/timeseries/v1.0/ts/"):

    # Convert the features to lat/lon (Note: this removes empty features!!!)

    dataframe = TSService.to_lat_lon(features_df)

    # For the time being: bail out. In the typical use case (different buffers around same area) this makes sense.

    if len(dataframe.index) != len(features_df.index):
        print("Features dataframe contains invalid geometries")
        return None

    # Convert dates to datetime date objects

    startdate = TSService._convert_date(startdate).date().isoformat()
    enddate   = TSService._convert_date(enddate).date().isoformat()
    params    = {"endDate": enddate, "startDate": startdate}

    # Prepare GeometryCollection. In shapely we trust.

    geometries = [mapping(dataframe.loc[fid, 'geometry']) for fid in dataframe.index]    
    geometrycollection = {"type" : "GeometryCollection", "geometries" : geometries}

    # Send a request to the web service

    headers = {'content-type': 'application/json'}
    url = endpoint + layername + '/geometries/histogram'
 
    service_response = requests.post(url, json=geometrycollection, params=params, headers=headers)

    if(service_response.status_code == 200):

        # Check if responsible response 

        try:
            response_json = service_response.json()

        except ValueError:
            print("Resulting json Invalid")
            print(service_response.text)
            return None

        #
        #    Apparently a we get a dict: { 'results' : { ... } }
        #
        if(len(response_json['results']) > 0):
            #
            #    its contents looks to be another dict: { '2017-05-04' : xxx, '2017-05-03' : yyy, ... }
            #    keys seem to be (all ?) dates from startdate till and including enddate - unordered
            #    values a list (non-empty) containing a list for each feasture, which can be empty or contain dicts {'value': aa, 'count': bb}, {'value': cc, 'count': dd}
            #
            #    {
            #        '2017-05-04': [ [], [] ], 
            #        '2017-05-02': [ [], [] ], 
            #        '2017-05-05': [ [], [] ], 
            #        '2017-05-07': [ [], [] ], 
            #        '2017-05-06': [ [{'count': 10, 'value': 4.0}, {'count': 1016, 'value': 5.0}], [{'count': 67, 'value': 4.0}, {'count': 1150, 'value': 5.0}]], 
            #        '2017-05-03': [ [{'count': 1026, 'value': 9.0}                             ], [{'count': 5, 'value': 8.0},  {'count': 1212, 'value': 9.0}]]
            #    }
            #
            dictofdataframes = {fid: None for fid in dataframe.index}
            #
            #    reformat results to be compatible with the plain get_histogram function
            #
            for fidx in range(len(dataframe.index)):
                series = []
                for date, listofhistograms in response_json['results'].items():
                    fidxhistogram = listofhistograms[fidx]
                    if fidxhistogram is not None:
                        data   = [bucket['count'] for bucket in fidxhistogram] 
                        index  = [float('nan') if bucket['value'] == 'NaN' else bucket['value'] for bucket in fidxhistogram]
                        series.append(pd.Series(data=data, name=date, index=index))
                histodataframe = pd.DataFrame(series).fillna(0)
                histodataframe.index = pd.to_datetime(histodataframe.index)
                dictofdataframes[dataframe.index[fidx]] = histodataframe
            return dictofdataframes

        else:
            print("Empty results")
            return None
    else:
        print("Received error for url: " + url + " Error message: " + service_response.text)
        return None
