import os
import pandas as pd
import requests
import geopandas as gpd
import tqdm

identifier = 'pid'

# Shapefile with the parcels
fields_file = '../testresources/belcam_all.geojson'
# Time range for the time series
start = pd.to_datetime("2017-04-01")
end = pd.to_datetime("2017-11-30")


################
# PROCESS
################

#retrieve list of unique dates inside our interval
all_dates = sorted(set([x[:10] for x in requests.get("https://proba-v-mep.esa.int/api/catalog/v2/CGS_S2_FAPAR/times").json() if pd.to_datetime(x).date() >= start.date() and pd.to_datetime(x).date()<=end.date()]))

parcels_df = gpd.read_file(fields_file)
parcels_df.set_index(identifier, inplace=True)

from dataclient.tsservice import TSService

index = pd.to_datetime(pd.Index(all_dates))


iterable = tqdm.tqdm(TSService().timeseries_async("S2_FAPAR",parcels_df,start,end,num_workers=8),total=len(parcels_df))
with open('my_csv.csv', 'a') as f:
    #write header
    pd.DataFrame(index).T.to_csv(f, header=False)
    for timeseries in iterable:
        timeseries = timeseries.reindex(index)
        pd.DataFrame(timeseries).T.to_csv(f, header=False)

