from unittest import TestCase, skip
import dataclient
from datetime import datetime
from dataclient.tsservice import TSService
from dateutil.parser import parse
import pandas as pd
import pytz
try:
    from tqdm import tqdm
    tqdm.pandas(desc="Progress")
except ImportError:
    pass

class TestTSService(TestCase):
    """This class provides unit tests for the internal PROBA-V MEP Python data client"""
    def test_s2_local(self):
        layer = dataclient.s2_fapar_10m(("user", "invalid"))
        self.assertIsNotNone(layer)
        filename = 'testresources/belcam_test.geojson'
        threads = 1

        self.dataframe_timeseries(filename, layer, threads)

    def dataframe_timeseries(self, filename, layer, threads):
        import geopandas as gpd
        df = gpd.read_file(filename)
        start = parse("2017-01-01T00:00Z")
        end = parse("2017-01-31T00:00Z")
        result = layer.timeseries(df, startdate=start, enddate=end, threads=threads)
        self.assertEqual(len(df), len(result))
        self.assertGreaterEqual(result.columns.min().replace(tzinfo=pytz.utc), pd.to_datetime(start))
        self.assertLessEqual(result.columns.max().replace(tzinfo=pytz.utc), pd.to_datetime(end))
        return (df,result)

    def test_s2_parallel(self):
        layer = dataclient.s2_fapar_10m(("user", "invalid"))
        self.assertIsNotNone(layer)
        self.dataframe_timeseries('testresources/belcam_all.geojson', layer, 2)

    def test_s2_single_histogram(self):

        import json
        series = pd.Series(name="MyField")
        geometry = {'type': 'Polygon', 'coordinates': [[[4.6770629938691854, 50.82172692290532], [4.6550903376191854, 50.80697613242405], [4.6866760309785604, 50.797429020705295], [4.7196350153535604, 50.795692972629176], [4.7402343805879354, 50.81738893871384], [4.6770629938691854, 50.82172692290532]]]}
        #2 options for providing geometry:
        series['geometry_json'] = json.dumps(geometry)#geometry as geojson string
        #series['geometry'] = "{}"#geometry as shapely object
        result = dataclient.get_histogram(series,"S2_FAPAR", parse("2017-01-01T00:00Z"), parse("2017-01-31T00:00Z"))
        print(result)

    def test_s2_single(self):
        from dataclient.tsservice import get_timeseries
        import json
        series = pd.Series(name="MyField")
        geometry = {'type': 'Polygon', 'coordinates': [[[4.6770629938691854, 50.82172692290532], [4.6550903376191854, 50.80697613242405], [4.6866760309785604, 50.797429020705295], [4.7196350153535604, 50.795692972629176], [4.7402343805879354, 50.81738893871384], [4.6770629938691854, 50.82172692290532]]]}
        #2 options for providing geometry:
        series['geometry_json'] = json.dumps(geometry)#geometry as geojson string
        #series['geometry'] = "{}"#geometry as shapely object
        result = get_timeseries(series,"S2_FAPAR", parse("2017-01-01T00:00Z"), parse("2017-01-31T00:00Z"))
        print(result)

    def test_s2_single_single_date(self):
        from dataclient.tsservice import get_timeseries
        import json
        series = pd.Series(name="MyField")
        geometry = {'type': 'Polygon', 'coordinates': [[[4.6770629938691854, 50.82172692290532], [4.6550903376191854, 50.80697613242405], [4.6866760309785604, 50.797429020705295], [4.7196350153535604, 50.795692972629176], [4.7402343805879354, 50.81738893871384], [4.6770629938691854, 50.82172692290532]]]}
        #2 options for providing geometry:
        series['geometry_json'] = json.dumps(geometry)#geometry as geojson string
        #series['geometry'] = "{}"#geometry as shapely object
        result = get_timeseries(series,"S2_FAPAR", parse("2017-01-03T00:00Z"), parse("2017-01-03T00:00Z"))
        print(result)

    def test_s2_single_empty_series_error(self):
        from dataclient.tsservice import get_timeseries
        import json
        series = pd.Series(name="MyField")
        geometry = {'type': 'Polygon', 'coordinates': [[[4.6770629938691854, 50.82172692290532], [4.6550903376191854, 50.80697613242405], [4.6866760309785604, 50.797429020705295], [4.7196350153535604, 50.795692972629176], [4.7402343805879354, 50.81738893871384], [4.6770629938691854, 50.82172692290532]]]}
        #2 options for providing geometry:
        series['geometry_json'] = json.dumps(geometry)#geometry as geojson string
        #series['geometry'] = "{}"#geometry as shapely object
        with self.assertRaises(IOError):
            result = get_timeseries(series,"S2_FAPAR", parse("2010-01-01T00:00Z"), parse("2010-01-01T00:00Z"))
            print(result)

    def test_s2_single_defaults(self):

        from dataclient.tsservice import get_timeseries
        import json
        series = pd.Series(name="MyField")
        geometry = {'type': 'Polygon', 'coordinates': [[[4.6770629938691854, 50.82172692290532], [4.6550903376191854, 50.80697613242405], [4.6866760309785604, 50.797429020705295], [4.7196350153535604, 50.795692972629176], [4.7402343805879354, 50.81738893871384], [4.6770629938691854, 50.82172692290532]]]}

        series['geometry'] = geometry

        result = get_timeseries(series,"S2_FAPAR")
        print(result)

    def test_s2_async(self):

        import geopandas as gpd
        df = gpd.read_file('testresources/belcam_all.geojson')
        start = parse("2017-01-01T00:00Z")
        end = parse("2017-01-31T00:00Z")
        iterable = TSService().timeseries_async("S2_FAPAR",df,start,end)
        for i in iterable:
            print(i)

    def test_features(self):
        import geopandas as gpd
        import numpy as np
        #input = "/mnt/ceph/Projects/Belcam/fields/geojson/2017_fields/mai_2017_Flanders.geojson"
        #index = "pid"
        input = "testresources/belcam_all.geojson"
        index = "BID"
        df = gpd.read_file(input)
        df.index = df[index]

        start = parse("2017-01-01T00:00Z")
        end = parse("2017-12-31T00:00Z")
        layer = dataclient.s2_fapar_10m(("user", "invalid"))

        result = layer.timeseries(df, startdate=start, enddate=end, threads=1,single_request=True,endpoint="http://192.168.205.137:8080/v1.0/ts/")
        self.assertEqual(len(df),len(result))
        averages = result.applymap(lambda cell: cell['average'])
        print(averages)
        averages.to_csv("all.csv")

    def test_histogram_pixel_counts(self):
        import pandas as pd
        import json
        import numpy

        series = pd.Series(name="MyField")

        geometry = {'type': 'Polygon', 'coordinates': [
            [[4.6770629938691854, 50.82172692290532], [4.6550903376191854, 50.80697613242405],
             [4.6866760309785604, 50.797429020705295], [4.7196350153535604, 50.795692972629176],
             [4.7402343805879354, 50.81738893871384], [4.6770629938691854, 50.82172692290532]]]}

        series['geometry_json'] = json.dumps(geometry)  # geometry as geojson string

        result = dataclient.get_histogram(series, "S2_FAPAR_CLOUDCOVER_SC", parse("2017-01-01T00:00Z"),
                                          parse("2017-01-31T00:00Z"), endpoint="http://192.168.205.137:8080/v1.0/ts/")

        self.assertEqual('float64', result.columns.dtype)
        self.assertTrue(any(numpy.isnan(value) for value in result.columns.values))

        pixel_counts = result.sum(axis=1)
        self.assertEqual(1, pixel_counts.unique().size)

    def test_get_timeseries_n_features(self):

        import pandas as pd
        import geopandas as gpd
        import numpy as np
        import shapely.geometry
        # input = "/mnt/ceph/Projects/Belcam/fields/geojson/2017_fields/mai_2017_Flanders.geojson"
        # index = "pid"
        input = "testresources/belcam_all.geojson"
        index = "BID"
        df = gpd.read_file(input)
        df.index = df[index]

        df2 = df.to_crs({'init': 'epsg:32631'})
        df2.geometry = df2.buffer(-100) # This will created some empty polys
        df = df2.to_crs(df.crs)

        start = parse("2017-01-01T00:00Z", ignoretz=True)
        end = parse("2017-01-31T00:00Z", ignoretz=True)
        layer = "S2_FAPAR"

        result = dataclient.get_timeseries_n_features(df, layer, start, end,
                                                      endpoint="http://192.168.205.137:8080/v1.0/ts/")

        # The result should have a result for every feature
        self.assertEqual(len(df), len(result))

        for id in df.index:

            # Every feature ID should be present in the result
            self.assertIn(id, result)

            # Always None for empty features
            if df.loc[id].geometry.is_empty:
                self.assertIsNone(result[id])

            # Maybe None for empty series (totalCount=0 for every date)
            if result[id] is not None:

                # Should a pandas Series
                self.assertIsInstance(result[id], pd.Series)

                # With feature ID as name
                self.assertEqual(result[id].name, id)

                # A within the specified date range
                self.assertGreaterEqual(result[id].index[0], start)
                self.assertLessEqual(result[id].index[-1], end)

    @skip
    def test_geometries_histogram(self):
        """
        Basic test for behaviour of geometries/histogram with overlapping polygons
        EP-2905
        :return:
        """
        params =  {'endDate': '2017-07-01', 'startDate': '2017-06-01'}
        url =  "http://tsviewer-rest-test.vgt.vito.be:8080/v1.0/ts/S2_SCENECLASSIFICATION/geometries/histogram"
        import json
        with open('../testresources/geometrycollection.geojson','r+') as f:
            geometrycollection = json.load(f)

        print('headers: %s' % (headers))
        print('params: %s' % (params))
        print('url: %s' % (url))
        print('geometrycollection: %s' % (geometrycollection))

        import requests
        service_response = requests.post(url, json=geometrycollection, params=params)

        if (service_response.status_code == 200):

            # Check if responsible response


            response_json = service_response.json()
            print(response_json)

    def test_get_timeseries_multiband(self):

        from dataclient.tsservice import get_timeseries_multiband
        import json

        import pandas as pd
        import geopandas as gpd
        import numpy as np
        import shapely.geometry
        # input = "/mnt/ceph/Projects/Belcam/fields/geojson/2017_fields/mai_2017_Flanders.geojson"
        # index = "pid"
        input = "testresources/belcam_all.geojson"
        index = "BID"
        df = gpd.read_file(input)
        df = df.to_crs({'init': 'epsg:4326'})
        df.index = df[index]

        start = parse("2017-01-01T00:00Z", ignoretz=True)
        end = parse("2017-01-31T00:00Z", ignoretz=True)

        result = get_timeseries_multiband(df.iloc[0],
                                          "S1_GRD_SIGMA0_DESCENDING",
                                          start,
                                          end,
                                          endpoint='http://192.168.205.137:8080/v1.0/ts/'
                                          )

        # Result should be a dict
        self.assertIsNotNone(result)
        self.assertIsInstance(result, dict)

        for bandname in ['VV', 'VH', 'ANGLE']:

            # Band should be present in the result
            self.assertIn(bandname, result)

            # Should a pandas Series
            self.assertIsInstance(result[bandname], pd.Series)

            # A within the specified date range
            self.assertGreaterEqual(result[bandname].index[0], start)
            self.assertLessEqual(result[bandname].index[-1], end)

        print(result)

    def setUp(self):
        self.tick = datetime.now()

    def tearDown(self):
        self.tock = datetime.now()
        diff = self.tock - self.tick
        print( str(diff.seconds) + " seconds" )