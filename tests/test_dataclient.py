from unittest import TestCase
import dataclient
from dateutil.parser import parse
import requests_mock
import pandas as pd
import os
from io import BytesIO

@requests_mock.mock()
class TestDataClient(TestCase):
    """This class provides unit tests for the internal PROBA-V MEP Python data client"""

    def find_testresource(self,file):
        import os
        test_path = os.path.dirname(os.path.realpath(__file__))
        return os.path.join(test_path,"..","testresources",file)

    def test_download_pixels(self,m):
        with open(self.find_testresource('PROBAV_NDVI_description.xml'), 'rb') as coverageinfo_file:
            m.get("https://proba-v-mep.esa.int/applications/geo-viewer/app/geoserver/ows?service=WCS&request=DescribeCoverage&version=2.0.1&coverageId=PV_MEP__PROBAV_S10_TOC_333M_NDVI",body=coverageinfo_file)
            with open(self.find_testresource('PV_MEP__PROBAV_S10_TOC_333M_NDVI-170321.tiff'), 'rb') as coverage:
                m.post("https://proba-v-mep.esa.int/applications/geo-viewer/app/geoserver/ows?service=WPS&request=execute",
                    headers={
                        "content-type":"image/tiff"
                    },
                    body=coverage)

                layer = dataclient.probav_s10_toc_300m(("username", "password"))
                self.assertIsNotNone(layer)
                triangle = {'coordinates': [[[4.553097726311535, 51.2838732204153],
                                                          [4.546832086052746, 51.2747460575773],
                                                          [4.563740731682628, 51.273994328049596],
                                                          [4.553097726311535, 51.2838732204153]]],
                                         'type': 'Polygon'}
                pixels_df = layer.pixels(triangle, startdate=parse("2017-01-20T00:00Z"),
                                      enddate=parse("2017-01-31T00:00Z"))
                self.assertEqual(pd.to_datetime("2017-01-21T00:00Z",utc=True),pixels_df.columns[0])
                print(pixels_df)

    def test_download(self,m):
        with open(self.find_testresource('PROBAV_NDVI_description.xml'), 'rb') as coverageinfo_file:
            m.get("https://proba-v-mep.esa.int/applications/geo-viewer/app/geoserver/ows?service=WCS&request=DescribeCoverage&version=2.0.1&coverageId=PV_MEP__PROBAV_S10_TOC_333M_NDVI",body=coverageinfo_file)
            with open(self.find_testresource('PV_MEP__PROBAV_S10_TOC_333M_NDVI-170321.tiff'), 'rb') as coverage:
                adapter = m.post("https://proba-v-mep.esa.int/applications/geo-viewer/app/geoserver/ows?service=WPS&request=execute",
                       headers={
                           "content-type": "image/tiff"
                       },
                       body=coverage)

                layer = dataclient.probav_s10_toc_300m(("username", "password"))
                self.assertIsNotNone(layer)
                triangle = {'coordinates': [[[4.553097726311535, 51.2838732204153],
                                             [4.546832086052746, 51.2747460575773],
                                             [4.563740731682628, 51.273994328049596],
                                             [4.553097726311535, 51.2838732204153]]],
                            'type': 'Polygon'}
                layer.download("download",triangle,startdate = parse("2017-03-21T00:00Z"), enddate =  parse("2017-03-21T00:00Z"),verbose = True)
                self.assertTrue(os.path.exists("download/PV_MEP__PROBAV_S10_TOC_333M_NDVI-170321.tiff"))
                print(adapter.last_request.text)
                with open(self.find_testresource("wps_execute.xml"),"r+") as execute_request:
                    expected_request = execute_request.read()
                    self.assertEqual(expected_request,adapter.last_request.text)


    def test_download_high_res(self,m):
        with open(self.find_testresource('fcover_description.xml'), 'rb') as coverageinfo_file:
            m.get("https://sentineldata.vito.be/ows?service=WCS&request=DescribeCoverage&version=2.0.1&coverageId=CGS__CGS_S2_FCOVER",body=coverageinfo_file)

            adapter = m.post("https://sentineldata.vito.be/ows?service=WPS&request=execute",
                   headers={
                       "content-type": "image/tiff"
                   },
                   body=BytesIO(b"test"))

            layer = dataclient.s2_fcover_10m(("username", "password"))
            self.assertIsNotNone(layer)
            triangle = {'coordinates': [[[4.553097726311535, 51.2838732204153],
                                         [4.546832086052746, 51.2747460575773],
                                         [4.563740731682628, 51.273994328049596],
                                         [4.553097726311535, 51.2838732204153]]],
                        'type': 'Polygon'}
            layer.download("download",triangle,startdate = parse("2018-01-10T10:34:07.000Z"), enddate =  parse("2018-01-10T10:34:07.000Z"),verbose = True,buffer_distance_pixels=2)
            self.assertTrue(os.path.exists("download/PV_MEP__PROBAV_S10_TOC_333M_NDVI-170321.tiff"))
            print(adapter.last_request.text)
            with open(self.find_testresource("wps_execute_fcover.xml"),"r+") as execute_request:
                expected_request = execute_request.read()
                self.assertEqual(expected_request,adapter.last_request.text)



    def test_download_bbox(self,m):
        with open(self.find_testresource('PROBAV_NDVI_description.xml'), 'rb') as coverageinfo_file:
            m.get("https://proba-v-mep.esa.int/applications/geo-viewer/app/geoserver/ows?service=WCS&request=DescribeCoverage&version=2.0.1&coverageId=PV_MEP__PROBAV_S10_TOC_333M_NDVI",body=coverageinfo_file)
            with open(self.find_testresource('PV_MEP__PROBAV_S10_TOC_333M_NDVI-170321.tiff'), 'rb') as coverage:
                m.post("https://proba-v-mep.esa.int/applications/geo-viewer/app/geoserver/ows?service=WCS&request=GetCoverage",
                       headers={
                           "content-type": "image/tiff"
                       },
                       body=coverage)
                layer = dataclient.probav_s10_toc_300m(("username", "password"))
                self.assertIsNotNone(layer)
                triangle = {'coordinates': [[[4.553097726311535, 51.2838732204153],
                                             [4.546832086052746, 51.2747460575773],
                                             [4.563740731682628, 51.273994328049596],
                                             [4.553097726311535, 51.2838732204153]]],
                            'type': 'Polygon'}
                layer.download_bbox("download",triangle,startdate = parse("2017-03-21T00:00Z"), enddate =  parse("2017-03-25T00:00Z"),verbose = True)

