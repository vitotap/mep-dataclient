import datetime
import json
from unittest import TestCase
from requests.exceptions import HTTPError
from mock import mock
import dataclient


def coverage_info_response(*args, **kwargs):
    with open('testresources/fcover_description.xml', 'r') as text_input:
        return MockedResponse(200, text_input.read())


class MockedResponse(object):
    """This class represents a mocked requests.Response object"""

    def __init__(self, status_code, text):
        self.content = text
        self.status_code = status_code

    def raise_for_status(self):
        raise HTTPError()

class TestCoverageInfo(TestCase):
    @mock.patch('requests.get', side_effect=coverage_info_response)
    def test_get_coverageinfo(self, mock_get):
        """Unit test for retrieval of producttypes."""
        coverage_info = dataclient.s2_fcover_10m(None).coverageInfo
        self.assertEqual(255,coverage_info.nodata)
        self.assertEqual("Lat",coverage_info.xAxis)
        self.assertEqual("Long",coverage_info.yAxis)
        self.assertEqual("time",coverage_info.timeAxis)