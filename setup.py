import re

from setuptools import setup

with open('dataclient/__init__.py', 'r') as fd:
    version = re.search(r'^__version__\s*=\s*[\'"]([^\'"]*)[\'"]',
                        fd.read(), re.MULTILINE).group(1)

setup(name='dataclient',
      version=version,
      author='Jeroen Dries',
      author_email='jeroen.dries@vito.be',
      description='Data client to extract specific regions of interest from the PROBA-V MEP Data Set. This client uses externally accessible web services, and is not restricted to running in a MEP environment.',
      url='',
      package_data = {
            'dataclient': ['*.xml'],
      },
      setup_requires=['pytest-runner'],
      tests_require=['pytest','mock','requests-mock==1.5.2'],
      packages=['dataclient'],
      test_suite = 'tests',
      install_requires=['requests', 'numpy', 'pandas', 'rasterio>=0.36.0', 'python-dateutil', 'Shapely', 'tqdm',
                        'geopandas', 'Fiona>=1.8a1']
      )
